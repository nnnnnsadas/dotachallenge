$(document).ready(function() {
    $("#share").click(function() {
        if ($("#sharelink").css('display') == "none")
        {
            $("#sharelink").fadeIn();
            $(this).animate({opacity: 0.5}, 100);
        }
        $("#sharelink").focus();
        $("#sharelink").select();
    });
    $("#sharelink").focusout(function() {
        $("#share").animate({opacity: 1}, 100);
        if ($("#sharelink").css('display') != "none")
            $(this).fadeOut();
    });
});
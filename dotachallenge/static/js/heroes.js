$(document).ready(function() {
    $("#rattletrap").attr("allias", "clockwerk");
    $("#zuus").attr("allias", "zeus");
    $("#shadow_shaman").attr("allias", "rhasta");
    $("#queenofpain").attr("allias", "queen of pain");
    $("#life_stealer").attr("allias", "naix");
    $("#drow_ranger").attr("allias", "traxex");
    $("#spirit_breaker").attr("allias", "barathrum");
    $("#nevermore").attr("allias", "shadow_fiend");
    $("#shredder").attr("allias", "timbersaw");
    $("#keeper_of_the_light").attr("allias", "ezalor");

    $("#heroes").find("img").mouseenter(function() { 
        var temp = $(this).attr('id');
        var pos = $(this).offset();
        var width = $(this).width();
        var height = $(this).height();
        $("#heroHover").attr("src", "http://cdn.dota2.com/apps/dota2/images/heroes/" + temp + "_hphover.png");
        $("#heroHover").css({"top": pos.top - height/2 + "px", "left": pos.left - width/2 + "px"});
        $("#heroHover").parent("a").attr("href", "/" + $(this).attr('id'));
        $("#heroHover").show();
    });
    $("#heroHover").mouseout(function() {
        $(this).hide();
    });
    $("#search").bind("change paste keyup", function() {
        $("img.hero").each(function() {
            if (((new RegExp($("#search").val(), 'i')).test($(this).attr('id').replace("_", " ")) ||
                (new RegExp($("#search").val(), 'i')).test($(this).attr('allias'))) == false)  
                $(this).hide();
            else
                $(this).show();
        })
    }).keypress(function (e) {
        $("#heroHover").hide();
        if (e.which == 13) {
            var temp = $(this).val().replace(" ", "_").toLowerCase();
            if($("#" + temp).size()) {
                document.location.href = temp;
            }
            else if($("img.hero:visible").size() == 1)
                document.location.href = "/" + $("img.hero:visible").attr("id") + "/";
        }
    });
});
from flask import render_template
from flask import g
from flask import abort
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
from dotachallenge import app, models, oid, db
from os import urandom
from binascii import hexlify
import re
import urllib2
import random

_steam_id_re = re.compile('steamcommunity.com/openid/id/(.*?)$')

@app.before_request
def before_request():
    g.user = None
    if 'user_id' in session:
        g.user = models.User.query.get(session['user_id'])


@app.route('/logout')
def logout():
    session.pop('user_id', None)
    return redirect(oid.get_next_url())


@app.route('/login')
@oid.loginhandler
def login():
    if g.user is not None:
        return redirect(oid.get_next_url())
    return oid.try_login('http://steamcommunity.com/openid')


@oid.after_login
def create_or_login(resp):
    match = _steam_id_re.search(resp.identity_url)
    g.user = models.User.get_or_create(match.group(1))
    session['user_id'] = g.user.id
    return redirect(oid.get_next_url())


@app.route('/', methods=['GET'])
def heroes():
    heroes_names = []
    for i in models.Hero.query.all():
        heroes_names.append(i.name)
    return render_template('heroes.html', heroes = heroes_names)


@app.route('/<heroid>/', methods=['GET'])
def hero(heroid):
    heroes = [i.name for i in models.Hero.query.all()]
    if not(heroid in heroes):
        abort(404)
    items = models.Item.query.all()
    settings = {"OneBootsMin": True, "OneBootsMax": True, "OneOrbMax": False, "NoDublicateItems": True, 
                "AghanimsScepterForAll": True}
    if g.user:
        settings["OneBootsMin"] = g.user.OneBootsMin
        settings["OneBootsMax"] = g.user.OneBootsMax
        settings["OneOrbMax"] = g.user.OneOrbMax
        settings["NoDublicateItems"] = g.user.NoDublicateItems
        settings["AghanimsScepterForAll"] = g.user.AghanimsScepterForAll
    random.seed(long(hexlify(urandom(100)), 16))
    items_ = range(6)
    orbEffects = 0
    boots = 0
    aghanims_scepter = False
    for i in range(6):
        while True:
            temp = random.choice(items)
            if not(temp in items_) or settings["NoDublicateItems"] == False:
                items_[i] = temp
                if items_[i].boots:
                    boots+=1
                elif items_[i].orb:
                    orbEffects+=1
                break
    if settings["OneBootsMin"] and boots == 0:
        boots_ = []
        for i in items:
            if i.boots:
                boots_.append(i)
        items_[random.randint(0,5)] = random.choice(boots_)
    if settings["OneBootsMax"] and boots > 1:
        for i in range(6):
            if boots == 1:
                break
            elif items_[i].boots:
                temp = random.choice(items)
                while temp.boots:
                    temp = random.choice(items)
                items_[i] = temp
                boots-=1
    if heroid == 'pudge' or heroid == 'vengefulspirit':
        for i in range(6):
            while items_[i].name == 'blink':
                items_[i] = random.choice(items)
    if settings["OneOrbMax"]:
        maxOrb = 1
        if models.Hero.query.filter(models.Hero.name == heroid).first().hasOrb:
            maxOrb = 0
        for i in range(6):
            if items_[i].orb and orbEffects != maxOrb:
                temp = random.choice(items)
                while temp.orb:
                    temp = random.choice(items)
                items_[i] = temp
                orbEffect-=1
            elif orbEffects == maxOrb:
                break
    itemsid_ = ""
    for i in range(6):
        if items_[i].id < 10:
            temp = "00" + str(items_[i].id)
            itemsid_ = itemsid_ + temp
        elif items_[i].id < 100:
            temp = "0" + str(items_[i].id)
            itemsid_ = itemsid_ + temp
        else:
            itemsid_ = itemsid_ + str(items_[i].id)
    items__ = [i.name for i in items_]
    return render_template('hero.html', heroid = heroid, items = items__, itemsid = itemsid_)


@app.route('/<heroid>/<stri>/')
def share(heroid, stri):
    items = range(6)
    for i in range(6):
        items[i] = models.Item.query.get(int(stri[(i)*3: (i+1)*3]))
    for i in range(6):
        items[i] = items[i].name
    return render_template('hero.html', heroid = heroid, items = items, itemsid = stri)


@app.route('/random/')
def randomhero():
    return redirect('/' + random.choice([i.name for i in models.Hero.query.all()]) + '/')


@app.route('/setup/', methods=['GET', 'POST'])
def setup():
    if request.method == 'POST':
        u = g.user
        u.OneBootsMin = bool(request.form.getlist("OneBootsMin"))
        u.OneBootsMax = bool(request.form.getlist('OneBootsMax'))
        u.OneOrbMax = bool(request.form.getlist('OneOrbMax'))
        u.NoDublicateItems = bool(request.form.getlist('NoDublicateItems'))
        u.AghanimsScepterForAll = bool(request.form.getlist('AghanimsScepterForAll'))
        db.session.commit()
    return redirect(request.form['next'])

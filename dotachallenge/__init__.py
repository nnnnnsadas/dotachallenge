from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.openid import OpenID

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
oid = OpenID(app)
app.secret_key = 'sak dik mne sdelal blya'

def get_url():
    global app
    return app.config['URL']

app.jinja_env.globals.update(get_url=get_url)

from dotachallenge import views, models
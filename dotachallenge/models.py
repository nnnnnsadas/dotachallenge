from dotachallenge import db, app
from werkzeug import url_encode
import hashlib
import urllib2
import json


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    steam_id = db.Column(db.String(40))
    nickname = db.Column(db.String(80))
    OneBootsMin = db.Column(db.Boolean, default=True)
    OneBootsMax = db.Column(db.Boolean, default=True)
    OneOrbMax = db.Column(db.Boolean, default=False)
    NoDublicateItems = db.Column(db.Boolean, default=True)
    AghanimsScepterForAll = db.Column(db.Boolean, default=True)

    @staticmethod
    def get_or_create(steam_id):
        rv = User.query.filter_by(steam_id=steam_id).first()
        if rv is None:
            rv = User()
            rv.steam_id = steam_id
            steamdata = get_steam_userinfo(rv.steam_id)
            rv.nickname = steamdata['personaname']
            db.session.add(rv)
            db.session.commit()
        steamdata = get_steam_userinfo(rv.steam_id)
        rv.nickname = steamdata['personaname']
        return rv


class Item(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(64), index = True, unique = True)
    orb = db.Column(db.Boolean, default = False)
    boots = db.Column(db.Boolean)

    def __repr__(self):
        return '<Item %r>' % (self.name)


class Hero(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(64), index = True, unique = True)
    hasOrb = db.Column(db.Boolean, default = False)
    hasAghanim = db.Column(db.Boolean, default = True)

    def __repr__(self):
        return '<Hero %r>' % (self.name)


def get_pass_hash(passw):
    return hashlib.sha512(passw).hexdigest()


def get_steam_userinfo(steam_id):
    options = {
        'key': app.config['STEAM_API_KEY'],
        'steamids': steam_id
    }
    url = 'http://api.steampowered.com/ISteamUser/' \
          'GetPlayerSummaries/v0001/?%s' % url_encode(options)
    rv = json.load(urllib2.urlopen(url))
    return rv['response']['players']['player'][0] or {}
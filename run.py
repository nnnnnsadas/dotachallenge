from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from dotachallenge import app
from daemon import Daemon
import sys, time

class TornadoDaemon(Daemon):
        def run(self):
                http_server = HTTPServer(WSGIContainer(app))
                http_server.listen(80)
                IOLoop.instance().start()

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
    # daemon = TornadoDaemon('/tmp/server-daemon.pid')

    # if len(sys.argv) == 2:
    #     if 'start' == sys.argv[1]:
    #         daemon.start()
    #     elif 'stop' == sys.argv[1]:
    #         daemon.stop()
    #     elif 'restart' == sys.argv[1]:
    #         daemon.restart()
    #     else:
    #         print "Unknown command"
    #         sys.exit(2)
    #     sys.exit(0)
    # else:
    #     print "usage: %s start|stop|restart" % sys.argv[0]
    #     sys.exit(2)
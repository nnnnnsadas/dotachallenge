from BeautifulSoup import BeautifulSoup
from dotachallenge import db, models
import urllib2


for i in models.Item.query.all():
    db.session.delete(i)
for i in models.Hero.query.all():
    db.session.delete(i)
db.session.commit()
response = urllib2.urlopen('http://www.dota2.com/heroes/')
soup = BeautifulSoup(''.join(response))
temp = soup.findAll('img', {"class":"heroHoverLarge"})
heroes_ = temp
for index, i in enumerate(temp):
    heroes_[index] = i['id'].replace("hover_", "")
for i in heroes_:
    h = models.Hero(name=i)
    db.session.add(h)
db.session.commit()

response = urllib2.urlopen('http://www.dota2.com/items/')
soup = BeautifulSoup(''.join(response))
items_ = soup.findAll("div", {"class":"shopColumn"})
for i in range(len(items_)):
    items_[i] = items_[i].findAll('div')
    for j in range(len(items_[i])):
        items_[i][j] = items_[i][j]['itemname']
columns = soup.findAll("div", {"class":"shopColumn"})
columns[:] = [i.findAll("img", {"class":"shopColumnHeaderImg"}) for i in columns]
for i in range(len(columns)):
    for j in range(len(columns[i])):
        columns[i][j] = columns[i][j]['alt']
items__ = items_
items_ = dict()
for i in range(len(columns)):
    items_[columns[i][0]] = items__[i]
# item groups -- CONSUMABLES, ATTRIBUTES, ARMAMENTS, ARCANE, COMMON, SUPPORT, CASTER, WEAPONS, ARMOR, ARTIFACTS, SECRET SHOP
items_ = items_['ARTIFACTS'] + items_['CASTER'] + items_['SUPPORT'] + items_['WEAPONS'] + items_['ARMOR']
boots_ = ["phase_boots", "travel_boots", "tranquil_boots", "arcane_boots", "power_treads"]
shit = ["buckler", "headdress", "yasha", "sange", "maelstrom", "helm_of_the_dominator", "ring_of_basilius", 
        "soul_booster", "hood_of_defiance", "basher", "lesser_crit"]
for i in boots_ + shit:
    if i in items_:
        items_.remove(i)
items_.append("hand_of_midas")
items_.append("blink")
items_.append("soul_ring")

for i in boots_:
    b = models.Item(name = i, boots = True)
    db.session.add(b)
for i in items_:
    it = models.Item(name = i, boots = False)
    db.session.add(it)
db.session.commit()